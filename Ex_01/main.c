#include <errno.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include "utils.h"

int create_server_socket(char *port) {
    int sockfd = 0;

    struct addrinfo hints = {};
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    struct addrinfo *addr;

    int status = 0;
    if ((status = getaddrinfo(NULL, port, &hints, &addr))) {
        exit(print_error(STDERR_FILENO, 2, "getaddrinfo error: %s",
                         gai_strerror(status)));
    }

    if ((sockfd = socket(addr->ai_family,
                         addr->ai_socktype,
                         addr->ai_protocol))
        == 0) {
        exit(print_error(STDERR_FILENO, 3, "socket error: %s\n",
                         strerror(errno)));
    }

    int optval = 1;
    setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));

    if (bind(sockfd, addr->ai_addr, addr->ai_addrlen) != 0)
        exit(print_error(STDERR_FILENO, 4, "bind error: %s\n",
                         strerror(errno)));

    if (listen(sockfd, 10) != 0)
        exit(print_error(STDERR_FILENO, 5, "listen error: %s\n",
                         strerror(errno)));

    freeaddrinfo(addr);

    return sockfd;
}

int manage_client_connection(const int sockfd, const struct sockaddr_storage
client) {
    char read[1024];
    while (recv(sockfd, read, 1024, 0) > 0) {
        char response[1035];
        sprintf(response, "> %s", read);
        send(sockfd, response, strlen(response), 0);
        memset(read, 0, sizeof(read) / sizeof(read[0]));
    }
    printf("Connection closed, exiting server.");
    return 0;
}

int run_server(const int sockfd) {
    struct sockaddr_storage client;
    socklen_t addr_size = sizeof client;
    int accept_fd = 0;
    if ((accept_fd = accept(sockfd, (struct sockaddr *) &client,
                            &addr_size)) < 0)
        print_error(STDERR_FILENO, 6, "accept error: %s\n",
                    strerror(errno));
    else {
        int status = manage_client_connection(accept_fd, client);
    }
    return 0;
}

int main(int argc, char *argv[]) {

    char *port;
    if (argc == 2) {
        char *endptr = NULL;
        if (strncmp("-h", argv[1], 2) == 0)
            exit(print_help(argv[0], 0));
        port = argv[1];
    } else {
        exit(print_help(argv[0], 1));
    }

    int sockfd = create_server_socket(port);

    int status = run_server(sockfd);
    close(sockfd);
    return status;
}
