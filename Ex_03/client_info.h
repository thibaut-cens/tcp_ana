#ifndef CLIENT_INFO
#define CLIENT_INFO
#define NCLIENTS 100

typedef struct client_data_s {
    short used;
    int sock;
    unsigned *nbclient;
    pthread_t thread;
    pthread_mutex_t* mutex;
} client_data;

struct client_data_s *get_next_unused(struct client_data_s array[]) {
    int i;
    for (i = 0;
         i < NCLIENTS;
         i++) {
        if (array[i].used == 0)
            return &(array[i]);
    }
    return NULL;
}

#endif