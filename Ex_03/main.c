#include <errno.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <pthread.h>
#include <fcntl.h>
#include "utils.h"
#include "client_info.h"
#include <signal.h>

int stop = 0;

int create_server_socket(char *port) {
    int sockfd = 0;

    struct addrinfo hints = {};
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    struct addrinfo *addr;

    int status = 0;
    if ((status = getaddrinfo(NULL, port, &hints, &addr))) {
        exit(print_error(STDERR_FILENO, 2, "getaddrinfo error: %s",
                         gai_strerror(status)));
    }

    if ((sockfd = socket(addr->ai_family,
                         addr->ai_socktype,
                         addr->ai_protocol))
        == 0) {
        exit(print_error(STDERR_FILENO, 3, "socket error: %s\n",
                         strerror(errno)));
    }

    int optval = 1;
    setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));

    if (bind(sockfd, addr->ai_addr, addr->ai_addrlen) != 0)
        exit(print_error(STDERR_FILENO, 4, "bind error: %s\n",
                         strerror(errno)));

    if (listen(sockfd, 10) != 0)
        exit(print_error(STDERR_FILENO, 5, "listen error: %s\n",
                         strerror(errno)));

    freeaddrinfo(addr);

    fcntl(sockfd, F_SETFL, O_NONBLOCK);

    return sockfd;
}

void *pthread_entry_point(void *voidargs) {
    client_data *args = (client_data *) voidargs;
    printf("Created thread: {sockfd:%d, nbclient: %d, used: %d}\n",
           args->sock,
           *args->nbclient,
           args->used);
    char buff[1024];
    int read = 0;
    while ((read = recv(args->sock, buff, 1024, 0)) > 0) {
        char answer[1025] = ">";
        strncat(answer, buff, read);
        answer[sizeof(buff) + 1] = 0;
        send(args->sock, answer, strlen(answer) + 1, 0);
    }
    pthread_mutex_lock(args->mutex);
    close(args->sock);
    (*args->nbclient)--;
    args->used = 0;
    args->sock = -1;
    pthread_mutex_unlock(args->mutex);
    printf("Exiting thread: {sockfd:%d, nbclient: %d, used: %d}\n",
           args->sock,
           *args->nbclient,
           args->used);
    return NULL;
}

int run_server(const int sockfd) {
    client_data cdata_array[NCLIENTS];

    //Creating an initialising local mutex (Global are ugly)
    pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
    for (unsigned i = 0; i < NCLIENTS; i++) {
        cdata_array[i].mutex = &mutex;
    }
    unsigned nbclient = 0;
    short first_connection = 0;

    //Creating file descriptor set for select
    fd_set readfds;
    FD_ZERO(&readfds);
    struct timeval timeout = {1, 0};

    do {
        FD_SET(sockfd, &readfds);

        struct sockaddr_storage client;
        socklen_t addr_size = sizeof client;
        int accept_fd = 0;

        select(sockfd + 1, &readfds, NULL, NULL, &timeout);

        if (FD_ISSET(sockfd, &readfds)) {
            if ((accept_fd = accept(sockfd, (struct sockaddr *) &client,
                                    &addr_size)) < 0)
                print_error(STDERR_FILENO, 6, "accept error: %s\n",
                            strerror(errno));
            else {
                first_connection = 1;
                if (nbclient < NCLIENTS) {
                    client_data *cdata = get_next_unused(cdata_array);
                    pthread_mutex_lock(cdata->mutex);
                    cdata->sock = accept_fd;
                    cdata->nbclient = &nbclient;
                    nbclient++;
                    cdata->used = 1;
                    if (pthread_create(&(cdata->thread), NULL,
                                       pthread_entry_point,
                                       cdata))
                        print_error(STDERR_FILENO, 0,
                                    "Failed to create thread.");
                    pthread_detach(cdata->thread);
                    pthread_mutex_unlock(cdata->mutex);
                }
            }
        }

    } while (nbclient > 0 || first_connection == 0);
    printf("All connection closed, exiting...\n");

    return 0;
}

int main(int argc, char *argv[]) {

    char *port;
    if (argc == 2) {
        char *endptr = NULL;
        if (strncmp("-h", argv[1], 2) == 0)
            exit(print_help(argv[0], 0));
        port = argv[1];
    } else {
        exit(print_help(argv[0], 1));
    }

    int sockfd = create_server_socket(port);

    int status = run_server(sockfd);
    close(sockfd);
    return status;
}
