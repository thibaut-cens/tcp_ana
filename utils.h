#ifndef UTILS_HEADER
#define UTILS_HEADER

#define READ_LENGTH 1024

#include <stdarg.h>
#include <stdio.h>
#include <unistd.h>

int print_error(const int fd, const int return_code, const char *format, ...) {
    va_list argptr;
    va_start(argptr, format);

    vdprintf(fd, format, argptr);

    return return_code;
}

int print_help(char *self, int return_code) {
    printf("%s [-h | port]\n", self);
    printf("-h: Show this message\n");
    printf("port: Server port\n");
    return return_code;
}

#else
#endif
